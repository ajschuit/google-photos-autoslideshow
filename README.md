# Google Photos AutoSlideshow 

I really enjoy a lot about Google Photos, but one thing I found lacking was its ability to run a slideshow (like in a digital photo frame style).  
I thought that turning an album in Google Photos into a slideshow ought to be basic functionality.  
Since it appears it's not possible, I created this Google Apps Script project to make it simple to run a slideshow from a Google Photos Album.

## How to Use

### Installing

1. Go to (Google Apps Script)[script.google.com] and select New Script.  
1. Change _Untitled Project_ in the top left to the title for your project.
1. Replace the starter code `function myFunction() {}` with the code from Code.gs here.
1. Go to _File -> New -> Script file_ and call it OAuth.
1. Copy in the code from OAuth.gs here.
1. Go to _File -> New -> HTML file_ and call it index.
1. Copy in the code from index.html here.
1. Go to the (Google Developer Console)[console.developers.google.com] and generate credentials for use of the Photos API.
1. Replace the `CLIENT_ID` and `CLIENT_SECRET` values at the top of the Code.gs file with your new credentials.((Check here for help))[https://developers.google.com/identity/protocols/OAuth2]
1. Deploy the project as a web app (_Publish -> Deploy as web app..._)

### Running the app

1. Go to the url generated in the last step of **Installing** above.
1. Click the _Authorize_ link. Click through to allow in the tab that pops up.
1. Close the authorization tab and refresh the original tab.
1. All of the albums you have in Google Photos should show. Clicking on one will begin a slideshow in fullscreen mode.
